build:
	sudo docker build -t hockyy/cpc-scoreboard-frontend:1.0.0 .
buildclean:
	sudo docker build --no-cache -t hockyy/cpc-scoreboard-frontend:1.0.0 .
push:
	sudo docker push hockyy/cpc-scoreboard-frontend:1.0.0
pull:
	sudo docker pull hockyy/cpc-scoreboard-frontend:1.0.0
run:
	sudo docker run -p 9998:80 --restart=always --name cpc-scoreboard-frontend -d hockyy/cpc-scoreboard-frontend:1.0.0
stop:
	sudo docker stop cpc-scoreboard-frontend
	sudo docker rm cpc-scoreboard-frontend
