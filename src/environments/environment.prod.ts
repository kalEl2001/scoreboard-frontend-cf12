export const environment = {
  production: true,
  eventName: 'Compfest 13',
  jcpcScoreboardAddress: 'http://0.0.0.0:9999/jcpc-scoreboard',
  jcpcContestantsAddress: 'http://0.0.0.0:9999/jcpc-contestants',
  scpcScoreboardAddress: 'http://0.0.0.0:9999/scpc-scoreboard',
  scpcContestantsAddress: 'http://0.0.0.0:9999/scpc-contestants'
};

// export const environment = {
//   production: true,
//   eventName: 'Compfest 13',
//   jcpcScoreboardAddress: 'https://cpc-api.compfest.id/jcpc-scoreboard',
//   jcpcContestantsAddress: 'https://cpc-api.compfest.id/jcpc-contestants',
//   scpcScoreboardAddress: 'https://cpc-api.compfest.id/scpc-scoreboard',
//   scpcContestantsAddress: 'https://cpc-api.compfest.id/scpc-contestants'
// };
