// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  eventName: 'Compfest 13',
  jcpcScoreboardAddress: 'http://0.0.0.0:9999/jcpc-scoreboard',
  jcpcContestantsAddress: 'http://0.0.0.0:9999/jcpc-contestants',
  scpcScoreboardAddress: 'http://0.0.0.0:9999/scpc-scoreboard',
  scpcContestantsAddress: 'http://0.0.0.0:9999/scpc-contestants'
};

// export const environment = {
//   production: false,
//   eventName: 'Compfest 13',
//   jcpcScoreboardAddress: 'https://cpc-api.compfest.id/jcpc-scoreboard',
//   jcpcContestantsAddress: 'https://cpc-api.compfest.id/jcpc-contestants',
//   scpcScoreboardAddress: 'https://cpc-api.compfest.id/scpc-scoreboard',
//   scpcContestantsAddress: 'https://cpc-api.compfest.id/scpc-contestants'
// };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
