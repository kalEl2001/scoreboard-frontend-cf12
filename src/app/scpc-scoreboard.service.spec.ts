import { TestBed } from '@angular/core/testing';

import { ScpcScoreboardService } from './scpc-scoreboard.service';

describe('ScpcScoreboardService', () => {
  let service: ScpcScoreboardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScpcScoreboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
