import { ScpcScoreboardComponent } from './scpc-scoreboard/scpc-scoreboard.component';
import { JcpcScoreboardComponent } from './jcpc-scoreboard/jcpc-scoreboard.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

const routerOptions: ExtraOptions = {
  anchorScrolling: 'enabled',
};


const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent
  },
  {
    path: 'jcpc',
    component: JcpcScoreboardComponent
  },
  {
    path: 'scpc',
    component: ScpcScoreboardComponent
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
