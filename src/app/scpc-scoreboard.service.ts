import { environment } from './../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScpcScoreboardService {

  constructor(private httpService: HttpClient) { }

  getScpcScoreboard() {

    return this.httpService.get(environment.scpcScoreboardAddress);

  }

  getScpcContestants() {

    return this.httpService.get(environment.scpcContestantsAddress);

  }

}
