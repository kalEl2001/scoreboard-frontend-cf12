import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class JcpcScoreboardService {

  constructor(private httpService: HttpClient) { }

  getJcpcScoreboard() {

    return this.httpService.get(environment.jcpcScoreboardAddress);

  }

  getJcpcContestants() {

    return this.httpService.get(environment.jcpcContestantsAddress);

  }

}
