import { TestBed } from '@angular/core/testing';

import { JcpcScoreboardService } from './jcpc-scoreboard.service';

describe('JcpcScoreboardService', () => {
  let service: JcpcScoreboardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JcpcScoreboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
