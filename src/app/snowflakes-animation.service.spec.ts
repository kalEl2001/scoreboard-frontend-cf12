import { TestBed } from '@angular/core/testing';

import { SnowflakesAnimationService } from './snowflakes-animation.service';

describe('SnowflakesAnimationService', () => {
  let service: SnowflakesAnimationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SnowflakesAnimationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
