import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScpcScoreboardComponent } from './scpc-scoreboard.component';

describe('ScpcScoreboardComponent', () => {
  let component: ScpcScoreboardComponent;
  let fixture: ComponentFixture<ScpcScoreboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScpcScoreboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScpcScoreboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
