import { SnowflakesAnimationService } from './../snowflakes-animation.service';
import { ScpcScoreboardService } from './../scpc-scoreboard.service';
import { environment } from './../../environments/environment';
import { NavigationBarService } from './../navigation-bar.service';
import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-scpc-scoreboard',
  templateUrl: './scpc-scoreboard.component.html',
  styleUrls: ['./scpc-scoreboard.component.css']
})
export class ScpcScoreboardComponent implements OnInit {

  scoreboardData: any;
  contestantsData: any;

  teamName: string;
  teamInstitution: string;
  teamMember1: string;
  teamMember2: string;
  teamMember3: string;

  numOfProb: number;

  interval: any;
  intervalBetweenUpdates = 30000;

  previousRanking = [];
  rankDiff = [];
  updatedTime: Date;

  title = 'SCPC Scoreboard | ' + environment.eventName.toUpperCase();

  upArrow = "/assets/img/up-arrow.png";
  downArrow = "/assets/img/down-arrow.png";
  noDiff = "/assets/img/no-diff.png";

  ac = "#32CD32C0";
  firstAc = "#228B22C0";
  wa = "#DC0000C0";
  pending = "#4287F5C0"

  constructor(public nav: NavigationBarService, public snow: SnowflakesAnimationService, private scpcScoreboardService: ScpcScoreboardService, private titleService: Title) { }

  ngOnInit(): void {

    this.titleService.setTitle(this.title);

    this.nav.show();

    this.scpcScoreboardService.getScpcContestants().subscribe(res => {

      this.contestantsData = res;

    });

    this.scpcScoreboardService.getScpcScoreboard().subscribe(res => {

      this.scoreboardData = res;

      if (this.scoreboardData.type == "FROZEN") {
        this.snow.show();
      } else {
        this.snow.hide();
      }

      this.numOfProb = this.scoreboardData.scoreboard.state.problemJids.length;

      for (let i = 0; i < this.scoreboardData.scoreboard.content.entries.length; i++) {

        this.previousRanking.push({
          "contestantJid": this.scoreboardData.scoreboard.content.entries[i].contestantJid,
          "contestantRank": this.scoreboardData.scoreboard.content.entries[i].rank
        });

        this.rankDiff.push({
          "contestantJid": this.scoreboardData.scoreboard.content.entries[i].contestantJid,
          "diff": 0
        });

      }

      this.updatedTime = new Date(this.scoreboardData.updatedTime);

    });

    this.interval = setInterval(() => {
      this.scpcScoreboardService.getScpcScoreboard().subscribe(res => {
        this.scoreboardData = res;
        this.updatedTime = new Date(this.scoreboardData.updatedTime);
        if (this.scoreboardData.type == "FROZEN") {
          this.snow.show();
        } else {
          this.snow.hide();
        }
      });
      this.updateDiff();
    }, this.intervalBetweenUpdates);

  }

  ngOnDestroy() {

    if (this.interval) {
      clearInterval(this.interval);
    }

  }

  getContestantName(Jid: string) {

    return this.contestantsData[Jid].name;

  }

  getContestantUniversity(Jid: string) {

    return this.contestantsData[Jid].institutionName;

  }

  getRankDiff(Jid: string) {

    let prevRank = this.previousRanking.filter(function (prev) {
      return prev.contestantJid == Jid;
    })[0].contestantRank;

    let currRank = this.scoreboardData.scoreboard.content.entries.filter(function (curr) {
      return curr.contestantJid == Jid;
    })[0].rank;

    let index = this.previousRanking.map(function (e) {
      return e.contestantJid;
    }).indexOf(Jid);

    this.previousRanking[index] = {
      "contestantJid": Jid,
      "contestantRank": currRank
    };

    return (currRank - prevRank);

  }

  getDiffImage(Jid: string) {

    let diff = this.rankDiff.filter(function (rank) {
      return rank.contestantJid == Jid;
    })[0].diff;

    if (diff > 0) return this.downArrow;
    if (diff < 0) return this.upArrow;

    return this.noDiff;

  }

  getAbsDiff(Jid: string) {

    let diff = this.rankDiff.filter(function (rank) {
      return rank.contestantJid == Jid;
    })[0].diff;

    return Math.abs(diff);

  }

  updateDiff() {

    for (let i = 0; i < this.scoreboardData.scoreboard.content.entries.length; i++) {
      let Jid = this.rankDiff[i].contestantJid;
      this.rankDiff[i].diff = this.getRankDiff(Jid);
    }

  }

  redVal(score: number) {

    if (score == 0) return 220;
    if (score <= (this.numOfProb / 2)) return 255;
    return 255 - ((score - (this.numOfProb / 2)) * 50);
  }

  greenVal(score: number) {

    if (score >= (this.numOfProb / 2)) return 255;
    return ((score / this.numOfProb) * 255);

  }

  selectTeam(Jid: string) {

    this.teamName = this.getContestantName(Jid);
    this.teamInstitution = this.getContestantUniversity(Jid);
    this.teamMember1 = this.contestantsData[Jid].members[0];
    this.teamMember2 = this.contestantsData[Jid].members[1];
    this.teamMember3 = this.contestantsData[Jid].members[2];

  }

}
