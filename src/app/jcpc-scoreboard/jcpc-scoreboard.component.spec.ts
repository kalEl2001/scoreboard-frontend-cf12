import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JcpcScoreboardComponent } from './jcpc-scoreboard.component';

describe('JcpcScoreboardComponent', () => {
  let component: JcpcScoreboardComponent;
  let fixture: ComponentFixture<JcpcScoreboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JcpcScoreboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JcpcScoreboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
