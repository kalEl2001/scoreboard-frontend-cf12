FROM node:14.9.0-alpine3.10 AS builder
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build:prod

FROM nginx:1.19.2-alpine
COPY --from=builder /app/dist/scoreboard-frontend-cf12 /usr/share/nginx/html